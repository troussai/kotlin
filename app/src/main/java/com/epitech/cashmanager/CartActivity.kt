package com.epitech.cashmanager

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.*
import android.widget.Button
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat

class CartActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cart)

        val toolbar = findViewById<Toolbar>(R.id.toolbar)

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        toolbar.navigationIcon = getDrawable(R.drawable.ic_settings_black_24dp)

        val layoutInflater: LayoutInflater = LayoutInflater.from(applicationContext)
        val buttonView: View = layoutInflater.inflate(
            R.layout.right_toolbar_button,
            null,
            false
        )
        val buttonShop : Button = buttonView.findViewById(R.id.my_button)
        val l1 = Toolbar.LayoutParams(Toolbar.LayoutParams.WRAP_CONTENT, Toolbar.LayoutParams.WRAP_CONTENT)
        l1.gravity = Gravity.END
        l1.setMargins(0, 0, 16, 0)
        buttonShop.layoutParams = l1
        buttonShop.text = resources.getString(R.string.shop)
        buttonShop.setTextColor(ContextCompat.getColor(this, R.color.colorWhite))
        buttonShop.setOnClickListener{
            finish()
        }
        toolbar.addView(buttonShop)

        val buyTop :View = findViewById(R.id.buy_top)
        val buyBottom :View = findViewById(R.id.buy_bottom)
        val buttonBuyTop :Button = buyTop.findViewById(R.id.button_buy)
        val buttonBuyBottom :Button = buyBottom.findViewById(R.id.button_buy)
        val funcBuy :()->Unit = {
            val paymentIntent = Intent(this, PaymentActivity::class.java)
            startActivity(paymentIntent)
        }

        buttonBuyTop.setOnClickListener{ funcBuy() }
        buttonBuyBottom.setOnClickListener{ funcBuy() }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                val settingsIntent = Intent(this, SettingsActivity::class.java)
                startActivity(settingsIntent)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}
