package com.epitech.cashmanager

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.*
import android.widget.Button
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat

class PaymentActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment)

        val toolbar = findViewById<Toolbar>(R.id.toolbar)

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        toolbar.navigationIcon = getDrawable(R.drawable.ic_settings_black_24dp)

        val layoutInflater: LayoutInflater = LayoutInflater.from(applicationContext)
        val buttonView: View = layoutInflater.inflate(
            R.layout.right_toolbar_button,
            null,
            false
        )
        val buttonBack : Button = buttonView.findViewById(R.id.my_button)
        val l1 = Toolbar.LayoutParams(Toolbar.LayoutParams.WRAP_CONTENT, Toolbar.LayoutParams.WRAP_CONTENT)
        l1.gravity = Gravity.END
        l1.setMargins(0, 0, 16, 0)
        buttonBack.layoutParams = l1
        buttonBack.text = resources.getString(R.string.back)
        buttonBack.setTextColor(ContextCompat.getColor(this, R.color.colorWhite))
        buttonBack.setOnClickListener{
            finish()
        }
        toolbar.addView(buttonBack)

        val buttonCheque: Button = findViewById(R.id.button_cheque)
        buttonCheque.setOnClickListener {
            val chequeIntent = Intent(this, ChequeActivity::class.java)
            startActivity(chequeIntent)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                val settingsIntent = Intent(this, SettingsActivity::class.java)
                startActivity(settingsIntent)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}
