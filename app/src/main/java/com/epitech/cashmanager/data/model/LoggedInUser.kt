package com.epitech.cashmanager.data.model

/**
 * Data class that captures user information for logged in users retrieved from LoginRepository
 */
data class LoggedInUser(
    val userId: String,
    val displayName: String,
    val role: String,
    val email: String,
    val name: String,
    val token: String,
    val tokenType: String
)
