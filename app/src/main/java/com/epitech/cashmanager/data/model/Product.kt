package com.epitech.cashmanager.data.model

import java.util.*

data class Product (
        val id: UUID,
        val name: String,
        val path_image: String,
        val price: Float,
        val price_wt: Float
)