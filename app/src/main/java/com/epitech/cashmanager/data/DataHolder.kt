package com.epitech.cashmanager.data

import com.epitech.cashmanager.data.model.Cart
import com.epitech.cashmanager.data.model.LoggedInUser
import com.epitech.cashmanager.data.model.Settings

object DataHolder {
    var settings = Settings(100, 3, 3, "localhost:8080")
    var user: LoggedInUser? = null
    var cart: Cart? = null
}