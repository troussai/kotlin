package com.epitech.cashmanager.data.model

data class Settings (
        val connectionDelay: Int,
        val connexion_attemps: Int,
        val pays_attempts: Int,
        val ip_address: String
)