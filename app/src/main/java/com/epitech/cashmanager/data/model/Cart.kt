package com.epitech.cashmanager.data.model

import java.util.*

data class Cart (
        val id: UUID,
        val price: Float,
        val price_wt: Float,
        val products: List<Product>,
        val user: LoggedInUser
)