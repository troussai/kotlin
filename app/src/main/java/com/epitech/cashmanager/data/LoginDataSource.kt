package com.epitech.cashmanager.data

import android.util.Log
import com.epitech.cashmanager.data.model.LoggedInUser
import org.json.JSONObject
import java.io.IOException
import com.epitech.cashmanager.data.api.ApiConnector

/**
 * Class that handles authentication w/ login credentials and retrieves user information.
 */
class LoginDataSource {

    fun login(username: String, password: String): Result<LoggedInUser> {
        var user: LoggedInUser
        try {
            val dataResult: JSONObject = ApiConnector.login(username,password)
            if (dataResult.has("error")) {
                throw Exception(dataResult.toString())
            }
            user = LoggedInUser(java.util.UUID.randomUUID().toString(), "", "", "", username, dataResult.getString("accessToken"), dataResult.getString("tokenType"))
            DataHolder.user = user
            val userObject = ApiConnector.getUser(username)
            user = LoggedInUser(userObject.getString("id"), userObject.getString("name"), "", userObject.getString("email"), username, dataResult.getString("accessToken"), dataResult.getString("tokenType"))
            Log.d(null, user.toString())
        } catch (e: Throwable) {
            return Result.Error(IOException("Error logging in", e))
        }
        return Result.Success(user)
    }

    fun logout() {
        ApiConnector.logout()
    }
}

