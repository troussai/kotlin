package com.epitech.cashmanager.data.api

import android.util.Log
import com.epitech.cashmanager.data.DataHolder
import com.epitech.cashmanager.data.model.LoggedInUser
import com.epitech.cashmanager.data.model.Product
import org.json.JSONObject

object ApiConnector {

    /**
     * Function to call login api
     */

    fun login(username: String, password: String): JSONObject {
        try {

            var response = khttp.post(
                "http://${DataHolder.settings.ip_address}/signin",
                data = mapOf(
                    "userName" to username,
                    "password" to password
                )
            )
            return response.jsonObject
        } catch (e: Throwable) {
            Log.e(null, e.message)
            return JSONObject()
        }
    }

    /**
     * Function to call api to logout
     */
    fun logout() {
        khttp.post("http://${DataHolder.settings.ip_address}/signout", headers=mapOf("Authorization" to "${DataHolder.user!!.tokenType} ${DataHolder.user!!.token}"))
    }

    /**
     * Function to call api to get
     */
    fun getProductList(): JSONObject {
        var response = khttp.get("http://${DataHolder.settings.ip_address}/api/products", headers=mapOf("Authorization" to "${DataHolder.user!!.tokenType} ${DataHolder.user!!.token}"))
        return response.jsonObject
    }

    /**
     * Function to get juste one product
     */
    fun getProduct(product_id: Int): JSONObject {
        var response = khttp.get("http://${DataHolder.settings.ip_address}/api/products/".plus(product_id), headers=mapOf("Authorization" to "${DataHolder.user!!.tokenType} ${DataHolder.user!!.token}"))
        return response.jsonObject
    }

    /**
     * Function to get a User
     */
    fun getUser(username: String): JSONObject {
        var response = khttp.get("http://${DataHolder.settings.ip_address}/api/users/".plus(username), headers=mapOf("Authorization" to "${DataHolder.user!!.tokenType} ${DataHolder.user!!.token}"))
        return response.jsonObject
    }

    /**
     * Function to get all Users
     */
    fun getAllUsers(): JSONObject {
        var response = khttp.get("http://${DataHolder.settings.ip_address}/api/users", headers=mapOf("Authorization" to "${DataHolder.user!!.tokenType} ${DataHolder.user!!.token}"))
        return response.jsonObject
    }

    /**
     * Function do add product to card
     */
    fun addProductToCard(product: Product, user: LoggedInUser): JSONObject {
        /**
         * To do create class product and USer
         */
        var response = khttp.post(
            "http://${DataHolder.settings.ip_address}/api/card/addProduct",
            data = mapOf(
                "user" to user,
                "product" to product
            ),
            headers=mapOf("Authorization" to "${DataHolder.user!!.tokenType} ${DataHolder.user!!.token}")
        )
        return response.jsonObject
    }

    /**
     * Function to remove product from card
     */
    fun deleteProductFrom(product: Product, user: LoggedInUser): JSONObject {
        /**
         * Todo implement user class and product class
         */
        var response = khttp.post(
            "http://${DataHolder.settings.ip_address}/api/card/removeProduct",
            data = mapOf(
                "user" to product,
                "product" to user
            ),
            headers=mapOf("Authorization" to "${DataHolder.user!!.tokenType} ${DataHolder.user!!.token}")
        )
        return response.jsonObject
    }

    /**
     * Function to get card
     */
    fun getCard(id_user: Int): JSONObject {
        var response = khttp.get("http://${DataHolder.settings.ip_address}/api/card/fromUser/".plus(id_user), headers=mapOf("Authorization" to "${DataHolder.user!!.tokenType} ${DataHolder.user!!.token}"))
        return response.jsonObject
    }

}