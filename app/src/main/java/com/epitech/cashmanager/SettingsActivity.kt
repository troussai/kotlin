package com.epitech.cashmanager

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import com.epitech.cashmanager.data.DataHolder
import kotlinx.android.synthetic.main.activity_settings.*

class SettingsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)
        setSupportActionBar(findViewById(R.id.toolbar))

        val fragmentAdapter = TabAdapter(supportFragmentManager)
        viewpager_settings.adapter = fragmentAdapter

        tabs_settings.setupWithViewPager(viewpager_settings)

        val cancel = findViewById<Button>(R.id.buttonCancel)

        cancel.setOnClickListener{
            finish()
        }
    }
}
