package com.epitech.cashmanager

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.DisplayMetrics
import android.view.*
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.widget.Toolbar
import androidx.camera.core.*
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.LifecycleOwner
import com.epitech.cashmanager.qrCode.QrCodeAnalyzer
import com.google.firebase.ml.vision.barcode.FirebaseVisionBarcode
import java.util.concurrent.Executors


class ChequeActivity : AppCompatActivity() {

    companion object {
        private const val REQUEST_CAMERA_PERMISSION = 10
    }

    private lateinit var lastQr: FirebaseVisionBarcode
    private lateinit var textureView: TextureView
    private val executor = Executors.newSingleThreadExecutor()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cheque)

        textureView = findViewById(R.id.texture_view)
        val toolbar = findViewById<Toolbar>(R.id.toolbar)

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        toolbar.navigationIcon = getDrawable(R.drawable.ic_settings_black_24dp)

        val layoutInflater: LayoutInflater = LayoutInflater.from(applicationContext)
        val buttonView: View = layoutInflater.inflate(
            R.layout.right_toolbar_button,
            null,
            false
        )
        val buttonBack : Button = buttonView.findViewById(R.id.my_button)
        val l1 = Toolbar.LayoutParams(Toolbar.LayoutParams.WRAP_CONTENT, Toolbar.LayoutParams.WRAP_CONTENT)
        l1.gravity = Gravity.END
        l1.setMargins(0, 0, 16, 0)
        buttonBack.layoutParams = l1
        buttonBack.text = resources.getString(R.string.back)
        buttonBack.setTextColor(ContextCompat.getColor(this, R.color.colorWhite))
        buttonBack.setOnClickListener{
            val paymentIntent = Intent(this, PaymentActivity::class.java)
            startActivity(paymentIntent)
        }
        toolbar.addView(buttonBack)

        val buttonCancel :Button = findViewById(R.id.button_cancel)
        buttonCancel.setOnClickListener{
            finish()
        }

        if (isCameraPermissionGranted()) {
            textureView.post { startCamera() }
        } else {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.CAMERA), REQUEST_CAMERA_PERMISSION)
        }
    }

    private fun startCamera() {
        val previewConfig = PreviewConfig.Builder()
            .setLensFacing(CameraX.LensFacing.BACK)
            .build()

        val preview = Preview(previewConfig)

        preview.setOnPreviewOutputUpdateListener { previewOutput ->
            val parent = textureView.parent as ViewGroup
            parent.removeView(textureView)
            textureView.surfaceTexture = previewOutput.surfaceTexture
            parent.addView(textureView, 0)
        }

        val imageAnalysisConfig = ImageAnalysisConfig.Builder().apply {
            setImageReaderMode(
                ImageAnalysis.ImageReaderMode.ACQUIRE_LATEST_IMAGE)
        }.build()
        val qrCodeAnalyzer = QrCodeAnalyzer { qrCodes ->
            qrCodes.forEach{
                if (!::lastQr.isInitialized || lastQr.rawValue != it.rawValue) {
                    Toast.makeText(this, "QR Code: ${it.rawValue}.", Toast.LENGTH_SHORT).show()
                    lastQr = it
                }
            }
        }
        val imageAnalysis = ImageAnalysis(imageAnalysisConfig).apply {
            setAnalyzer(executor, qrCodeAnalyzer)
        }

        CameraX.bindToLifecycle(this as LifecycleOwner, preview, imageAnalysis)
    }

    private fun isCameraPermissionGranted(): Boolean {
        val selfPermission = ContextCompat.checkSelfPermission(baseContext, android.Manifest.permission.CAMERA)
        return selfPermission == PackageManager.PERMISSION_GRANTED
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (requestCode == REQUEST_CAMERA_PERMISSION) {
            if (isCameraPermissionGranted()) {
                textureView.post { startCamera() }
            } else {
                Toast.makeText(this, "Camera permission is required.", Toast.LENGTH_SHORT).show()
                finish()
            }
        }
    }
}
